package dev.antoineadam.exomind.home

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import dev.antoineadam.exomind.R

class HomeFragment : Fragment(R.layout.fragment_home) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.home_button).setOnClickListener {
            navigateToWeatherReport()
        }
    }

    private fun navigateToWeatherReport() {
        findNavController().navigate(R.id.action_homeFragment_to_weatherReportFragment)
    }
}