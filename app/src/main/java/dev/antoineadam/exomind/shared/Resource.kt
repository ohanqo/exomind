package dev.antoineadam.exomind.shared

import dev.antoineadam.exomind.shared.network.ErrorResponseDTO

sealed class Resource<out T : Any> {
    data class Success<out T : Any>(val response: T) : Resource<T>()
    data class Error(val error: ErrorResponseDTO?) : Resource<Nothing>()
    object Loading : Resource<Nothing>()
    object None : Resource<Nothing>()
}
