package dev.antoineadam.exomind.shared.network

data class ErrorResponseDTO(
    val statusCode: Int,
    val message: String,
    val error: String
)