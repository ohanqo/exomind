package dev.antoineadam.exomind

import android.app.Application
import dev.antoineadam.exomind.report.reportModule
import dev.antoineadam.exomind.shared.network.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            fragmentFactory()

            modules(networkModule)
            modules(reportModule)
        }
    }
}