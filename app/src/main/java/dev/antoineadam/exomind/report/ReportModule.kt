package dev.antoineadam.exomind.report

import android.content.Context
import dev.antoineadam.exomind.report.ui.WeatherReportAdapter
import dev.antoineadam.exomind.report.ui.WeatherReportFragment
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val reportModule = module {
    single { get<Retrofit>().create(WeatherReportService::class.java) }
    single { WeatherReportRepository(get()) }
    single { LoadingMessageProvider(get()) }
    single { get<Context>().resources }
    viewModel { WeatherReportViewModel(get(), get()) }
    fragment { WeatherReportFragment(get(), get()) }
    single { WeatherReportAdapter() }
}