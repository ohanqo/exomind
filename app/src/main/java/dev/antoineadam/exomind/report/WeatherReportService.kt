package dev.antoineadam.exomind.report

import com.haroldadmin.cnradapter.NetworkResponse
import dev.antoineadam.exomind.report.dto.WeatherReport
import dev.antoineadam.exomind.shared.network.ErrorResponseDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherReportService {
    @GET("weather")
    suspend fun searchWeather(
        @Query("q") q: String,
        @Query("units") units: String = "metric"
    ): NetworkResponse<WeatherReport, ErrorResponseDTO>
}