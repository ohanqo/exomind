package dev.antoineadam.exomind.report

class WeatherReportRepository(private val service: WeatherReportService) {
    suspend fun searchWeather(city: String) = service.searchWeather(city)
}