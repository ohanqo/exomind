package dev.antoineadam.exomind.report.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.tapadoo.alerter.Alerter
import dev.antoineadam.exomind.R
import dev.antoineadam.exomind.databinding.FragmentWeatherReportBinding
import dev.antoineadam.exomind.report.WeatherReportViewModel
import dev.antoineadam.exomind.shared.DataBindingFragment
import dev.antoineadam.exomind.shared.Resource
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class WeatherReportFragment(
    private val model: WeatherReportViewModel,
    private val adapter: WeatherReportAdapter
) : DataBindingFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding<FragmentWeatherReportBinding>(
            R.layout.fragment_weather_report,
            container
        ).apply {
            viewModel = model
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<RecyclerView>(R.id.weather_report_recyclerview).adapter = adapter
        model.fetchReport()

        model.loadingState.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED).onEach { state ->
            if (state is Resource.Success) {
                adapter.submitList(model.reports)
            } else if (state is Resource.Error) {
                activity?.let {
                    val message = state.error?.message ?: getString(R.string.an_error_as_occurred)

                    Alerter.create(it)
                        .setText(message)
                        .setBackgroundColorRes(R.color.red_500)
                        .show()
                }
            }
        }.launchIn(lifecycleScope)
    }
}