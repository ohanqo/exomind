package dev.antoineadam.exomind.report

import android.content.res.Resources
import dev.antoineadam.exomind.R

class LoadingMessageProvider(resources: Resources) {
    private var index = 0
    private var messages = arrayOf<String>()

    init {
        messages = resources.getStringArray(R.array.loading_message)
    }

    fun getNextMessage(): String {
        if (index == messages.size - 1) index = 0 else index += 1
        return messages[index]
    }
}