package dev.antoineadam.exomind.report

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.haroldadmin.cnradapter.NetworkResponse
import dev.antoineadam.exomind.report.dto.WeatherReport
import dev.antoineadam.exomind.shared.Resource
import dev.antoineadam.exomind.shared.utils.Constant.MAX_SECONDS
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class WeatherReportViewModel(
    private val repository: WeatherReportRepository,
    private val loadingMessageProvider: LoadingMessageProvider
) : ViewModel() {
    val reports = arrayListOf<WeatherReport>()
    val progressBarPercentage = MutableStateFlow(0)
    val loadingState = MutableStateFlow<Resource<Unit>>(Resource.None)
    val currentLoadingMessage = MutableStateFlow("")
    private val cities = arrayListOf("Rennes", "Paris", "Nantes", "Bordeaux", "Lyon")
    private var counter = 0
    private var currentCityIndex = 0

    fun fetchReport() = viewModelScope.launch {
        reports.clear()
        counter = 0
        currentCityIndex = 0
        loadingState.value = Resource.Loading
        switchLoadingMessage()

        while (counter < MAX_SECONDS) {
            onTick()
            delay(1000)
            counter += 1
        }
    }

    private suspend fun onTick() {
        incrementProgressBar()

        if (counter % 10 == 0) fetchNextCityReport()
        if (counter % 6 == 0) switchLoadingMessage()
        if (counter == MAX_SECONDS - 1) loadingState.value = Resource.Success(Unit)
    }

    private suspend fun fetchNextCityReport() {
        if (currentCityIndex == cities.size) return

        val cityName = cities[currentCityIndex]

        when (val response = repository.searchWeather(cityName)) {
            is NetworkResponse.Success -> {
                Log.d("WeatherReportViewModel", "Report added for city: $cityName")
                reports.add(response.body)
            }
            is NetworkResponse.ServerError -> {
                loadingState.value = Resource.Error(response.body)
            }
            is NetworkResponse.UnknownError -> {
                loadingState.value = Resource.Error(null)
            }
        }

        currentCityIndex += 1
    }

    private fun switchLoadingMessage() {
        currentLoadingMessage.value = loadingMessageProvider.getNextMessage()
    }

    private suspend fun incrementProgressBar() {
        val value = (counter.toFloat() / MAX_SECONDS.toFloat()) * 100
        progressBarPercentage.emit(value.toInt())
    }
}