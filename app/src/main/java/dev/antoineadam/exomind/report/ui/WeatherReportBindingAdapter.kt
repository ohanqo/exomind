package dev.antoineadam.exomind.report.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import dev.antoineadam.exomind.report.dto.WeatherReport

@BindingAdapter("load")
fun load(imageView: ImageView, item: WeatherReport) {
    val iconURL = "https://openweathermap.org/img/wn/${item.weather.first().icon}@2x.png"
    imageView.load(iconURL)
}
