package dev.antoineadam.exomind.report.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import dev.antoineadam.exomind.databinding.ItemWeatherReportBinding
import dev.antoineadam.exomind.report.dto.WeatherReport

class WeatherReportAdapter :
    ListAdapter<WeatherReport, WeatherReportAdapter.ReportViewHolder>(DiffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemWeatherReportBinding.inflate(inflater, parent, false)
        return ReportViewHolder(binding)
    }

    override fun getItemCount(): Int = currentList.size

    override fun onBindViewHolder(holder: ReportViewHolder, position: Int) =
        holder.bind(getItem(position))

    inner class ReportViewHolder(private val binding: ItemWeatherReportBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: WeatherReport) {
            binding.report = item
            binding.executePendingBindings()
        }
    }

    object DiffUtilCallback : DiffUtil.ItemCallback<WeatherReport>() {
        override fun areItemsTheSame(oldItem: WeatherReport, newItem: WeatherReport): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: WeatherReport, newItem: WeatherReport): Boolean {
            return false
        }
    }
}